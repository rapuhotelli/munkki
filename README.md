```
Munkki 1.1
rapuhotelli <jukka@lahetkangas.net>
Pull a line from a sqlite table on a first-in-first-out basis. Will stop after all lines are used once. Slack
integration requires the WEBHOOK environment variable to be set.

USAGE:
    munkki [OPTIONS] <--reset|--get|--add <string>|--piped|--slack|-l>

FLAGS:
    -g, --get        Prints oldest available watercooler line and sets line as spent.
    -h, --help       Prints help information
    -l               Lists all current lines. -ll for unused lines.
    -p, --piped      Reads standard input and adds it as new lines.
    -r, --reset      Clears the list of oneliners.
    -s, --slack      Same as get but also posts it to Slack webhook with additional decor.
    -V, --version    Prints version information

OPTIONS:
    -a, --add <string>    Adds string argument as an unused line.
    
FILES:
The following files are expected to exist in the same directory with the executable:
    .env             For Slack webhook url variable. See .env.example.

    slack_preludes.txt
                     For random greetings before every Slack message. (Optional)


EXAMPLE:
Cronjob for 10am every Monday, Wednesday and Friday, save logs:
0 10 * * 1,3,5 /path/to/munkki -s >> /home/your_user/munkki.log 2>&1
```
