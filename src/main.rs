use clap::{App, Arg, ArgGroup};
use dotenv;
use munkki::{add_line, create_string, get_line, get_path, list_table, post_to_slack, reset};

const WEBHOOK_ENV: &'static str = "WEBHOOK";

fn main() {
    let all_options = vec!["reset", "get", "add", "piped", "slack", "list"];

    let matches = App::new("Munkki")
        .version("1.1")
        .author("rapuhotelli <jukka@lahetkangas.net>")
        .about("Pull a line from a sqlite table on a first-in-first-out basis. Will stop after all lines are used once. Slack integration requires the WEBHOOK environment variable to be set.")
        .arg(
            Arg::with_name("reset")
                .short("r")
                .long("reset")
                .help("Clears the list of oneliners."),
        )
        .arg(
            Arg::with_name("get")
                .short("g")
                .long("get")
                .help("Prints oldest available watercooler line and sets line as spent."),
        )
        .arg(
            Arg::with_name("add")
                .short("a")
                .long("add")
                .value_name("string")
                .help("Adds string argument as an unused line.")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("piped")
                .short("p")
                .long("piped")
                .help("Reads standard input and adds it as new lines."),
        )
        .arg(
            Arg::with_name("slack")
                .short("s")
                .long("slack")
                .help("Same as get but also posts it to Slack webhook with additional decor."),
        )
        .arg(
            Arg::with_name("list")
                .short("l")
                .multiple(true)
                .help("Lists all current lines. -ll for unused lines."),
        )
        .group(ArgGroup::with_name("mode")
            .args(&all_options)
            .required(true))
        .get_matches();

    if matches.is_present("reset") {
        reset()
    } else if matches.is_present("get") {
        let line_info = get_line();
        println!("{}", create_string(line_info))
    } else if matches.is_present("slack") {
        let env_path = get_path(".env");
        dotenv::from_path(env_path).ok();
        let webhook = std::env::var(WEBHOOK_ENV).unwrap();

        let line_info = get_line();
        let line_str = create_string(line_info);
        if !line_str.is_empty() {
            println!("{}", &line_str);
            post_to_slack(&line_str, webhook.as_str())
        }
    } else if matches.is_present("list") {
        match matches.occurrences_of("list") {
            1 => {
                println!("{}", list_table(true))
            }
            _ => {
                println!("{}", list_table(false))
            }
        }
    } else if let Some(add_arg) = matches.value_of("add") {
        add_line(add_arg.to_string())
    } else if matches.is_present("piped") {
        loop {
            let mut input = String::new();
            std::io::stdin()
                .read_line(&mut input)
                .expect("Could not read pipe");
            input = input.trim().to_string();
            if input == "" {
                break;
            }
            add_line(input);
        }
    }
}
