use rand::seq::SliceRandom;
use rand::thread_rng;
use rusqlite::{Connection, NO_PARAMS};
use serde_json::json;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::path::PathBuf;

pub struct WatercoolerLine {
    pub id: Option<i32>,
    pub line: String,
    pub last_used: Option<String>,
}

struct RemainingLines {
    count: u32,
}

pub struct LineInformation {
    line: WatercoolerLine,
    remaining: RemainingLines,
}

impl LineInformation {
    pub fn is_last(&self) -> bool {
        return self.remaining.count == 0;
    }
    pub fn print_line(&self) -> &str {
        return self.line.line.as_str();
    }
}

pub fn get_path(file: &str) -> PathBuf {
    let mut dir = std::env::current_exe().unwrap();
    dir.pop();
    dir.push(file);
    dir
}

fn init_table(conn: &Connection) {
    conn.execute(
        "create table if not exists watercooler_lines (
            id integer primary key autoincrement,
            line text not null,
            last_used timestamp default null
        )",
        NO_PARAMS,
    )
    .unwrap();
}

fn drop_table(conn: &Connection) {
    conn.execute("drop table if exists watercooler_lines", NO_PARAMS)
        .unwrap();
}

fn insert_line(conn: &Connection, wc_line: WatercoolerLine) {
    conn.execute(
        "insert into watercooler_lines (line) values (?)",
        &[wc_line.line],
    )
    .unwrap();
}

pub fn list_table(all: bool) -> String {
    let conn = init_db();

    let query = match all {
        true => "select id, line, last_used from watercooler_lines",
        false => "select id, line, last_used from watercooler_lines where last_used is null",
    };

    let mut stmt = conn.prepare(query).unwrap();
    let wc_iter_result = stmt
        .query_map(NO_PARAMS, |row| {
            Ok(WatercoolerLine {
                id: row.get(0)?,
                line: row.get(1)?,
                last_used: row.get(2)?,
            })
        })
        .unwrap();

    let wc_iter: Vec<WatercoolerLine> = wc_iter_result.filter_map(Result::ok).collect();

    let mut list_string = String::new();
    for wc in wc_iter {
        list_string = format!("{}\n{}", list_string, wc.line)
    }

    return list_string;
}

fn reset_db(conn: &Connection) {
    drop_table(&conn);
    init_table(&conn);
}

fn init_db() -> Connection {
    let conn = Connection::open(get_path("munkki.db")).unwrap();
    init_table(&conn);
    conn
}

pub fn reset() {
    reset_db(&init_db());
}

pub fn add_line(text: String) {
    let line = WatercoolerLine {
        id: None,
        line: text,
        last_used: None,
    };
    let conn = init_db();
    insert_line(&conn, line);
}

fn get_unused_count(conn: &Connection) -> RemainingLines {
    let get = "select count(*) from watercooler_lines where last_used is null;";
    let remaining = conn
        .query_row(get, NO_PARAMS, |row| {
            Ok(RemainingLines { count: row.get(0)? })
        })
        .unwrap();

    remaining
}

pub fn create_string(line: Option<LineInformation>) -> String {
    if let Some(line) = line {
        let mut second_line = String::new();

        if line.is_last() {
            second_line.push_str("(BTW multa on sitten läpät loppu. @rapuhotelli halp.)");
        }

        return format!("{} {}", line.print_line(), second_line);
    }
    return String::new();
}

pub fn get_line() -> Option<LineInformation> {
    let conn = init_db();
    let line = get_unused_wc_line(&conn);
    let remaining = get_unused_count(&conn);

    match line {
        None => None,
        Some(line) => Some(LineInformation { line, remaining }),
    }
}

fn get_unused_wc_line(conn: &Connection) -> Option<WatercoolerLine> {
    let get = "select id, line from watercooler_lines where last_used is null limit 1;";

    let line = conn.query_row(get, NO_PARAMS, |row| {
        Ok(WatercoolerLine {
            id: row.get(0)?,
            line: row.get(1)?,
            last_used: None,
        })
    });

    match line {
        Ok(wc_line) => {
            conn.execute(
                "update watercooler_lines set last_used = current_timestamp where id = ?;",
                &[wc_line.id.unwrap()],
            )
            .unwrap();
            Some(wc_line)
        }
        Err(e) => match e {
            rusqlite::Error::QueryReturnedNoRows => None,
            _ => panic!("Error querying stuff"),
        },
    }
}

fn get_random_heading() -> String {
    let file = File::open(get_path("slack_preludes.txt"));
    return match file {
        Ok(file) => {
            let mut preludes: Vec<String> = vec![];
            let br = BufReader::new(file);

            for line in br.lines() {
                preludes.push(line.unwrap());
            }
            let mut rng = thread_rng();
            let selection = preludes.choose(&mut rng).unwrap();
            String::from(selection.clone())
        }
        Err(_) => {
            println!("foo");
            return String::new();
        }
    };
}

pub fn post_to_slack(wc_line: &String, url: &str) {
    let client = reqwest::blocking::Client::new();
    let markdown_text = format!("{} \n>{}", get_random_heading(), wc_line.clone());
    let json_payload = json!({
        "blocks": [
            {
            "type": "section",
            "text": {
                "type": "mrkdwn",
                "text": markdown_text
            }
            }
        ]
    });
    client.post(url).json(&json_payload).send().unwrap();
}
